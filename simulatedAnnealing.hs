import System.Environment
import System.Random
import Database.HDBC
import Database.HDBC.Sqlite3
import Control.Monad
import Debug.Trace

type Path = [Int]
type Temp = Double

decreasingTRate = 0.81
batchSize = 40
epsilonTemp = 0.0001
epsilonP = 0.007
constantN = 100
epsilonT = 0.0001
epsilonA = 0.01
initInitialTemp = 0.3
acceptancePercentageParameter = 0.87

main = do
    (seed:fileName:_) <- getArgs
    contents <- readFile fileName
    let cities = toList contents
    normalizer <- bigM cities
    putStrLn "Calculating initial temperature..."
    t <- initialTemperature (mkStdGen 123456789)
                            cities
                            initInitialTemp acceptancePercentageParameter
                            (normalizer * 10)
                            (normalizer * fromIntegral (length cities))
    putStrLn $ "Initial temperature: " ++ show t
    putStrLn "Beginning simulated annealing..."
    res <- simulatedAnnealing (mkStdGen (read seed :: Int)) t cities
                                (normalizer * 10)
                                (normalizer * fromIntegral (length cities))
    putStrLn "Finished. Based on a mathematical model, I believe the best path is:"
    print res
    f <- isFeasible res
    appendFile "isIt.txt" $ show res ++ show f

{- Given a list of cities calculates the sum of all distances one to one -}
bigM :: Path -> IO Double
bigM l = do
    ls <- sequence [distance x y | x <- l, y <- l]
    return $ maximum ls

{- Query database for the distance between two given cities -}
distance :: Int -> -- id city1
                Int -> --id city2
                    IO Double -- distance
distance c1 c2 = do
    conn <- connectSqlite3 "hoc.sl3"
    r <- quickQuery' conn
        "SELECT distance FROM connections WHERE id_city_1= ? and id_city_2= ?"
        (prepareValues c1 c2)
    disconnect conn
    if null r then return 0 else return (fromSql (head $ head r) :: Double)

{- Sorts and converts integers to SQL values -}
prepareValues :: Int -> Int -> [SqlValue]
prepareValues n m
    | n > m = [iToSql m, iToSql n]
    | otherwise = [iToSql n, iToSql m]

{- Cost function -}
bigC :: Path ->
            Double -> -- Penalty for no connection.
                Double -> -- Normalizer
                    IO Double
bigC p plty m = do
    dd <- sequence $ fuse distance p
    return $ sum (myUpdate 0 plty dd) / m

{- Chicken and egg problem, asign penalty value where the penalty value is -}
myUpdate :: Double -> Double -> [Double] -> [Double]
myUpdate a b (x:xs) = if x == a then b:myUpdate a b xs else x:myUpdate a b xs
myUpdate _ _ [] = []

{- Apply function to every two consecutive elements -}
fuse :: (a -> a -> b) -> [a] -> [b]
fuse f [x,y] = [f x y]
fuse f (x:y:xs) = f x y : fuse f (y:xs)
fuse _ _ = error "error in fuse; prolly not enough args"

{- Transforms string to list of integers -}
toList :: String -> [Int]
toList s = read $ "[" ++ s ++ "]" :: [Int]

{- the one -}
simulatedAnnealing :: StdGen -- Standard Generator for necessary randoms
                     -> Temp
                        -> Path
                            -> Double -- Penalty
                                -> Double -- Normalizer
                                    -> IO Path
simulatedAnnealing g = simulatedAnnealing' g 9999999 0


{- underrated recursive function who "really" does all the work -}
simulatedAnnealing' :: StdGen
                        -> Double -- New batch temp
                            -> Double -- Previous batch temp
                                -> Temp
                                    -> Path
                                        -> Double -- Penalty
                                            -> Double -- Normalizer
                                                -> IO Path
simulatedAnnealing' g p p' 0 s plty m = return s
simulatedAnnealing' g p p' t s plty m = do
    (avgTofB, bestSol, g') <- workOnBatch g t s plty m
    if abs (p - p') > epsilonP
        then do
            simulatedAnnealing' g' avgTofB p t bestSol plty m
        else simulatedAnnealing' g' p 0 (phi t) bestSol plty m

{- Decreasing temp function-}
phi :: Temp -> Temp
phi t = trace ("T: " ++ show t) $if t < epsilonTemp then 0 else t * decreasingTRate

{- Swaps two cities in the path to generate a neighbor -}
randomNeighbor :: Path -> StdGen -> (Path, StdGen)
randomNeighbor p gen =
    let (m, gen') = randomR (0, length p - 2) gen  -- does not choose the last one
        (n, gen'') = randomR (1, length p - 1) gen' -- does not choose the first one
    in if m == n -- if this happens the list would grow.
        then (permuteList ((m+n+ (fst $ randomR (0,length p) gen')) `mod` (length p - 2)) n p, gen'')
        else (permuteList m n p, gen'')

{- Given two integers swap the element at index m with element at index n.-}
permuteList :: Int -> Int -> [a] -> [a]
permuteList 0 n l = if n == length l
    then last l : init (tail l) ++ [head l]
    else (l !! n) : take (n-1) (tail l) ++ [head l] ++ drop (n+1) l
permuteList m n l@(x:xs)
    | m > n = permuteList n m l
    | otherwise = x: permuteList (m-1) (n-1) xs

{- Just a pretty face -}
workOnBatch :: StdGen
                -> Temp
                    -> Path
                        -> Double -- penalty
                            -> Double -- normalizer
                                -> IO (Double, Path, StdGen)
workOnBatch g t p plty m = workOnBatch' g batchSize t p p (0::Double) plty m 0

{- Recursive worker -}
workOnBatch' :: StdGen
                -> Double -- Batch's counter
                    -> Temp
                        -> Path -- Batch origin
                            -> Path -- Best from batch
                                -> Double -- sum of costs until now
                                    -> Double -- penalty
                                        -> Double -- normalizer
                                            -> Double -- upper bound
                                                -> IO (Double, Path, StdGen)
workOnBatch' g _ _ _ s r _ _ 1200  = trace "Batch aborted." return (r/1200, s, g)
workOnBatch' g 0 _ _ s r _ _ _    = return (r/batchSize, s, g)
workOnBatch' g l t p s r plty m u = do
        fs' <- bigC s' plty m
        fp  <- bigC p plty m
        fs  <- bigC s plty m
        let coolest = if fs' <= fs then s' else s in
            if fs' <= fp + t
                then do
                    --inform u
                    appendFile "acceptedALT" (show fs' ++ " from " ++ show s' ++ "\n")
                    workOnBatch' g' (l-1) t s' coolest (r + fs') plty m (u+1)
                else workOnBatch' g' l t p coolest r plty m (u+1)
        where (s',g') = randomNeighbor p g

{- Tests what percentage of accepted path make it -}
acceptancePercentage :: StdGen -> Path -> Temp -> Double -> Double -> IO Double
acceptancePercentage g s t = accepted g s t 0.0 constantN

inform :: Double -> IO ()
inform n = do
    if floor n `mod` 37 == 0
        then putStrLn "I'm still alive"
        else if floor n `mod` 301 == 0
            then putStrLn "Don't kill me, please"
            else return()

{- Auxiliar recursive -}
accepted :: StdGen
            -> Path
                -> Temp
                    -> Double -- counter
                        -> Double -- inverse counter
                            -> Double -- penalty
                                -> Double -- normalizer
                                    -> IO Double
accepted g s t c 0 plty m = return ((c/constantN) :: Double)
accepted g s t c n plty m = do
    fs' <- bigC s' plty m
    fs  <- bigC s plty m
    if fs' <= fs + t
        then accepted g' s' t (c + 1) (n-1) plty m
        else accepted g' s' t c (n-1) plty m
    where (s', g') = randomNeighbor s g


{- Hardest fucking part -}
binarySearch :: StdGen
                -> Path
                    -> Temp
                        -> Temp
                            -> Double --accepted%
                                -> Double -- penalty
                                    -> Double -- normalizer
                                        -> IO Temp
binarySearch g s t1 t2 p plty m = do
    pA <- acceptancePercentage g s ((t1 - t2)/2) plty m
    binarySearch' g s t1 t2 p plty m pA


binarySearch' :: StdGen
                -> Path
                    -> Temp
                        -> Temp
                            -> Double -- accepted%
                                -> Double -- penalty
                                    -> Double -- normalizer
                                        -> Double -- accepted%
                                            -> IO Temp
binarySearch' g s t1 t2 p plty m pA
    | t1 - t2 < epsilonT = return tm
    | abs (p - pA) < epsilonA = return tm
    | pA < p = do
        pA' <- aP t1 tm
        bs t1 tm pA'
    | otherwise = do
        pA' <- aP tm t2
        bs tm t2 pA'
    where tm = (t1 + t2) / 2
          aP tem1 tem2 = acceptancePercentage g s ((tem1 - tem2)/2) plty m
          bs i f = binarySearch' g s i f p plty m


initialTemperature :: StdGen
                        -> Path
                            -> Temp
                                -> Double -- %
                                    -> Double -- penalty
                                        -> Double -- normalizer
                                            -> IO Temp
initialTemperature g s t p plty m = do
    pA <- acceptancePercentage g s t plty m
    if abs (p - pA) <= epsilonP then return t
        else if pA < p
            then do
                tT <- reachIt g s pA p 2.0 t plty m
                binarySearch g s (tT/2) tT p plty m
            else do
                tT <- reachIt g s p pA 0.5 t plty m
                binarySearch g s tT (2*tT) p plty m


reachIt :: StdGen
            -> Path
                -> Double -- %
                    -> Double -- %
                        -> Double -- increasing/decreasing factor
                            -> Temp
                                -> Double --penalty
                                    -> Double -- normalizer
                                        -> IO Temp
reachIt g s p1 p2 f t plty m
    | p1 > p2 = do
        p <- acceptancePercentage g s (f*t) plty m
        reachIt g s p p2 f (f*t) plty m
    |otherwise = return t

{- Yup, because it ran for hours to end up with an unfeasible solution. -}
isFeasible :: Path -> IO Bool
isFeasible p = do
    dd <- sequence $ fuse distance p
    return $ all (/=0) dd