#!/bin/zsh

gnuplot <<- EOF
        set key off
        set size ratio 0.3
        set xlabel "Non-Negative Integers"
        set ylabel "Big C"
        set title "The Functional Way"   
        set term svg background rgb 'white'
        set output "$1.svg"
        plot "$1" with lines lc rgb "#00BE05" lw 0.1
EOF


